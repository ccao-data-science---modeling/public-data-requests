**Instructions for requester:** This issue ticket will help you clearly articulate your data request. Please read this template carefully, and populate each section as completely as you can.

#### General Description

> Please use this space to describe, in your own words, the data you are requesting.

#### Use Case

> Often, it is helpful for us to understand how the data will be used. In this space, please describe your use-case.

#### Universe and Unit of Observation

> Please describe the universe of the data you are requesting. Some examples include:
> 
> 1) All single-family PINs in Lyons township
> 2) Every unique combination of postal address and PIN in the City of Chicago
> 3) Every PIN in a Special Service Area in the City of Chicago
>
> Please describe the unit of observation for the data you are requesting. A unit of observation is the unique identifier for each row in your data. If you are requesting data that is unique by PIN, please state that.

#### Restrictions and Exclusions

> Please describe any specific restrictions or exclusions from your data. For example, if you defined your universe as "single-family", you should define your expectations about what that means. For example, do you consider a condominium a single-family property?

#### Citation and Use of Assessor Data

By submitting this issue ticket, I certify the following:

* I will cite the Cook County Assessor's Office. Publications and other materials leveraging this data should clearly and appropriately cite the CCAO, and *this particular issue ticket.*
* I will use this data to do good. 

- [ ] I agree to the above terms.

@ccaojardine @dfsnow
