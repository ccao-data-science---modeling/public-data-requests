# see https://gitlab.com/ccao-data-science---modeling/reports/public-data-requests/-/issues/6

# load libraries
library(dplyr)
library(tidyr)
library(stringr)
library(DBI)
library(odbc)
library(openxlsx)
library(here)

# create CCAODATA connection object
CCAODATA <- dbConnect(odbc(), .connection_string = Sys.getenv("DB_CONFIG_CCAODATA"))

# retrieve data
appeal591s <- dbGetQuery(CCAODATA, "

  --- DECLARE COLUMNS
  SELECT T.PIN, T.HD_CLASS AS [CLASS], T.TAX_YEAR AS [YEAR], township_name AS TOWNSHIP,
  T.HD_ASS_BLD + T.HD_ASS_BLD AS [PRE APPEAL AV],
  TB.HD_ASS_BLD + TB.HD_ASS_BLD AS [POST APPEAL AV],
  (T.HD_ASS_BLD + T.HD_ASS_BLD) - (TB.HD_ASS_BLD + TB.HD_ASS_BLD) AS REDUCTION,
  PC_DESC_CD3A, PC_DESC_CD3B

  --- PRE APPEAL VALUES
  FROM AS_HEADT T

  --- POST APPEALS VALUES
  LEFT JOIN AS_HEADTB TB
  ON T.PIN = TB.PIN AND T.TAX_YEAR = TB.TAX_YEAR

  --- APPEAL REASONS
  LEFT JOIN APPEALSDATA AD
  ON T.PIN = AD.PIN AND T.TAX_YEAR = AD.TAX_YEAR

  --- TOWNCODES FOR LIMITING OUTPUT
  LEFT JOIN FTBL_TOWNCODES TWNS
  ON LEFT(T.HD_TOWN, 2) = TWNS.township_code

  --- LIMIT OUTPUT BASED ON REQUEST (WEST CHICAGO ONLY)
  WHERE T.TAX_YEAR = 2020
  AND T.HD_CLASS = 591
  AND (PC_DESC_CD3A IS NOT NULL OR PC_DESC_CD3B IS NOT NULL)
  AND township_code = 77

  --- EXCLUDE ANY OBSERVATION THAT WASN'T ACTUALLY ADJUSTED
  AND T.HD_ASS_BLD + T.HD_ASS_BLD != TB.HD_ASS_BLD + TB.HD_ASS_BLD

") %>%

  # remove white space from appeal reasons
  dplyr::mutate(across(c(PC_DESC_CD3A, PC_DESC_CD3B), stringr::str_squish)) %>%

  # only keep observations with appeals having to do with vacancy
  dplyr::filter(grepl("occupancy|vacancy", PC_DESC_CD3A, ignore.case = TRUE) |
                  grepl("occupancy|vacancy", PC_DESC_CD3B, ignore.case = TRUE)) %>%

  # clean reason columns
  tidyr::unite("Appeal Reason", PC_DESC_CD3A:PC_DESC_CD3B, sep = " ", remove = TRUE)

# output data
openxlsx::write.xlsx(appeal591s, here("output", "pdr_6.xlsx"))
